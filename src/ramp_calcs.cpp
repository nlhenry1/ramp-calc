// Run calcs to find proper launch speed and ramp heights to land in the middle of the catch ramp.

#include <iostream>
#include <math.h>
int main(void)
{
    const float PI = 3.14159;
    const float DEG2RAD = PI / 180.0;
    const float FPS2MPH = 0.681818;
    const float tol = 0.1;
    const float time_limit = 100.0;
    const float dt = 0.01;
    const float accel_gravity = -32.174;
    int run_again_input = 0.0;
    bool run_again = true;

    std::cout << "DISCLAIMER: THIS TOOL ASSUMES NO DRAG, THUS ONLY USEFUL FOR SHORT-ISH JUMPS!" << std::endl;
    std::cout << std::endl;

    while(run_again)
    {
        float y_pos = 0.0, x_pos = 0.0, ground_height = 0.0, ramp_length = 0.0, ramp_height = 0.0,  agl = 0.0;
        float vel = 0.0, x_vel = 0.0, y_vel = 0.0;
        float x_targ = 0.0, ramp_angle = 0.0, ramp_x_pos = 0.0;
        float air_time = 0.0;
        float inc = 10.0;
        float error = 0.0, error_lp = 0.0;
        bool over_ramp = false;

        std::cout << "Enter Desired horizontal distance between launch/catch ramps in ft" << std::endl;
        std::cin >> ramp_x_pos;
        std::cout << "Enter Desired Launch/Landing Ramp Angle in deg" << std::endl;
        std::cin >> ramp_angle;
        std::cout << "Enter Landing Ramp Horizontal Length in ft" << std::endl;
        std::cin >> ramp_length;

        x_targ = ramp_x_pos + ramp_length/2.0; //Land in middle of catch ramp

        bool converged = false;
        float exec_time = 0.0;
        // Converge on launch speed necessary to deliver desired distance when height above ground (agl) == 0
        while(!converged && exec_time < time_limit)
        {
            ramp_height = ramp_length*tanf(ramp_angle*DEG2RAD);
            y_vel = vel*sinf(ramp_angle*DEG2RAD);
            x_vel = vel*cosf(ramp_angle*DEG2RAD);
            x_pos = 0.0;
            y_pos = ramp_height; // iteration starts with vehicle just leaving launch ramp
            air_time = 0.0;
            do
            {
                y_vel += accel_gravity * dt;
                y_pos += y_vel * dt;
                x_pos += x_vel * dt;

                over_ramp = (x_pos > (x_targ - ramp_length/2.0)) &&
                            (x_pos < (x_targ + ramp_length/2.0));
                
                if(over_ramp) ground_height = ramp_height - (x_pos - ramp_x_pos)*tanf(ramp_angle*DEG2RAD);
                else ground_height = 0.0;

                agl = y_pos - ground_height;
                exec_time += dt;
                air_time += dt;
            }
            while (agl > 0.0);

            error = x_targ-x_pos;

            if(fabs(error) < tol) converged = true;
            else if( (error < 0.0 && error_lp > 0.0) || 
                     (error > 0.0 && error_lp < 0.0))  inc /= -2.0; // If error switches signs, go other way in smaller increments
            
            error_lp = error;
                     
            vel += inc;
        }

        if(exec_time >= time_limit)
        {
            std::cout << "simulation time limit exceeded, not converged. Try different ramp angle" << std::endl;
        }
        else 
        {
            std::cout << "Launch/Catch Ramp Height = " << ramp_height << " ft" << std::endl;
            std::cout << "Required launch speed (to land in middle of catch ramp) = " << vel*FPS2MPH << " MPH" << std::endl;
            std::cout << "Air time = " << air_time << " sec" << std::endl;
            //std::cout << "simulated time for convergence= " << exec_time << std::endl;
        }

    std::cout << "Run again with different numbers? 1 = Yes, 0 = No" << std::endl;    
    std::cin >> run_again_input;
    run_again = (bool) run_again_input;
    }
    return 0;
}